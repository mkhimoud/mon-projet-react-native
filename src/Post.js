import React from 'react'
import { Text, Image, View, StyleSheet, TouchableOpacity } from 'react-native'
import { Divider } from 'react-native-elements'

const postFooterIcons = [
  {
    name: 'Like',
    imageUrl:'https://cdn-icons-png.flaticon.com/512/1077/1077035.png',
    
    likedImageUrl:'https://static.freeimages.com/images/home/filetypes/photo.png',
  },
  {
    name: 'Comment',
    imageUrl:'https://www.pinclipart.com/picdir/middle/571-5717511_sneak-peek-clip-art.png',
    likedImageUrl:'https://www.pinclipart.com/picdir/middle/571-5717511_sneak-peek-clip-art.png',
  },
  {
    name: 'Share',
    imageUrl:'https://cdn-icons-png.flaticon.com/128/340/340913.png',
    likedImageUrl:'https://static.freeimages.com/images/home/filetypes/photo.png',
  },
  {
    name: 'Save',
    imageUrl:'https://cdn3.iconfinder.com/data/icons/instagram-latest/1000/Instagram_save_archive-512.png',
    likedImageUrl:'https://static.freeimages.com/images/home/filetypes/photo.png',
  },
]


const  Post = ({post})=> {
  return(
    <View style={{ marginBottom: 30 }}>
      <Divider width={1} orientation='vertical'/>
      <PostHeader post={post} /> 
      <PostImage  post={post} /> 
      <View style={{marginHorizontal: 15,marginTop: 10 }}>
        <PostFooter />  
        <Likes post={post} /> 
        <Caption post={post} />         
      </View>
    </View>
  )
}
const PostHeader = ({post}) => (
  <View style={styles.pub}>
    <View style={{flexDirection: 'row', alignItems: 'center' }}>
    <Image style={styles.story} source={{uri:post.profile_picture}} />
    <Text style={{marginLeft:5}}>{post.user}</Text>
    </View>
    <TouchableOpacity>
    <Text style={{fontWeight: '900'}}>...</Text>
    </TouchableOpacity>
  </View>  
)

const PostImage = ({ post }) => (   
   <View style={{ width:'100%', height:300}} > 
    <Image  
    source={{ uri: post.imageUrl }}
    style={{ height:'100%', resizeMode: 'cover'}} 
    /> 
    </View>               
)
const PostFooter = () =>(
  <View style={{ flexDirection: 'row' }}>
    <View style={styles.leftFooterIconsContainer}>
      <Icon imgStyle={styles.footerIcon} imgUrl={postFooterIcons[0].imageUrl} />
      <Icon imgStyle={styles.footerIcon} imgUrl={postFooterIcons[1].imageUrl} />
      <Icon imgStyle={styles.footerIcon} imgUrl={postFooterIcons[2].imageUrl} />
    </View>
    <View style={{ flex:1, alignItems: 'flex-end'}}>
      <Icon imgStyle={styles.footerIcon} imgUrl={postFooterIcons[3].imageUrl} />
    </View>
  </View>
)
const Icon = ({imgStyle, imgUrl}) =>(
  <TouchableOpacity>
    <Image style={imgStyle} source={{ uri: imgUrl }} />
  </TouchableOpacity>
)
const Likes = ({post}) => (
  <View style={{ flexDirection:'row', margin:4}}>
      <Text style= {{ fontWeight: '600'}}>
      {post.likes.toLocaleString('en')} likes
      </Text>
    </View>
)
const Caption = ({post}) => (
<View style= {{ marginTop:5}}>
<Text>
  <Text style= {{ fontWeight: '600'}}>{post.user}</Text>
  <Text>  {post.caption}</Text>
</Text>
</View>
)


const styles = StyleSheet.create({
  story: {
      width:35,
      height:35,
      borderRadius: 35,
      marginLeft: 8,
      borderWidth:1.7,
      borderColor:'#ff8501'
  },
  icon: {
    width: 35,
    height:35,
    marginleft: 0,
    paddingLeft:10,
    
},
pub: { flexDirection: 'row',
 justifyContent: 'space-between', 
 margin:5, 
 alignItems:'center',
},
footerIcon: {
  width: 33,
  height: 33,
},
leftFooterIconsContainer: {
  flexDirection:'row',
  width:'32%',
  justifyContent: 'space-between',
},

})

export default Post;
