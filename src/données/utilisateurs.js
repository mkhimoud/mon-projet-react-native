

export const utilisateurs = [
{
    user : 'Massinissa',
    image:
    'https://i.pinimg.com/550x/24/f8/4c/24f84ce0ba7f43607570a4795e051457.jpg'
},
{
    user : 'Gabriel',
    image:
    'https://i.pinimg.com/236x/a0/5a/19/a05a19921c1da03e5875081e2c852683.jpg'
},
{
    user : 'Raphaël',
    image:
    'https://images.ctfassets.net/hrltx12pl8hq/a2hkMAaruSQ8haQZ4rBL9/8ff4a6f289b9ca3f4e6474f29793a74a/nature-image-for-website.jpg?fit=fill&w=480&h=320'
},
{
    user : 'Louis',
    image:
    'https://i.pinimg.com/236x/cb/e8/f6/cbe8f6ecbe1e3ec8c90e04b25d716b44--mens-fashion.jpg'
},
{
    user : 'Adam',
    image:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/640px-Image_created_with_a_mobile_phone.png'
},
]