import React, {Component} from 'react' 
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native'

const HomeScreen = () => {  
    return(
        <View style={Styles.container}>
            <TouchableOpacity>          
                <Image style={Styles.logo} source={require('./images/instgram.png')}
                />                                   
            </TouchableOpacity> 

            <View style={Styles.iconsContainer}>
                <TouchableOpacity>          
                    <Image style={Styles.icon} source={require('./images/kyiv-ukraine-er-août-icône-ajouter-un-nouveau-contenu-en-ligne-noire-élément-de-média-instagram-populaire-plus-suivre-le-228162071.jpg')}
                    />                                   
                </TouchableOpacity> 

                <TouchableOpacity>          
                    <Image style={Styles.icon} source={require('./images/images.png')} 
                    />                                   
                </TouchableOpacity> 

                <TouchableOpacity>
                    <View style={Styles.unreadBadge}>
                        <Text style={Styles.unreadbageText}>13</Text>
                    </View>          
                    <Image style={Styles.icon} source={require('./images/depositphotos_421121214-stock-illustration-direct-messages-button-icon-isolated.webp')}
                    />                                   
                </TouchableOpacity> 
            </View>
        </View>               
    )       
} 
const Styles = StyleSheet.create({
    unreadBadge: {
        backgroundColor:'red',
        position: 'absolute',
        left:20,
        bottom:18,
        width: 25,
        height:18,
        borderRadius:25,
        alignItems: 'center',
        justifyContent:'center',
        zIndex:100
    },
    unreadbageText:{
        fontWeight:'600'
    },
    icon: {
        fontWeight:'600',
        width: 37,
        height:37,
        marginleft: 10,
        resizeMode:'contain'
    },
    iconsContainer: {
        flexDirection:'row',
    },
    container: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal:20,
    },
    logo: {      
        width: 100,
        height:50,
        resizeMode:'contain'
    },
})
export default HomeScreen