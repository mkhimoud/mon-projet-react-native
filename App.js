import  React  from 'react' 
import {Text, View, SafeAreaView, StyleSheet, ScrollView} from 'react-native'
import HomeScreen from './src/HomeScreen'
import Post from './src/Post'
import  BottomTabs, { bottomTabIcons } from './src/BottomTabs'
import Stories from './src/Stories'
import { POSTS } from './src/données/posts'


const Home = () => {  
    return(
        <SafeAreaView style={styles.container}>                
           <HomeScreen /> 
           <Stories />          
           <ScrollView>
            {
             POSTS.map((post, index) =>(
               <Post post={post} key={index}/>  
             ))
            }                
           </ScrollView> 
           <BottomTabs icons={bottomTabIcons} />    
        </SafeAreaView>       
    )
}


const styles = StyleSheet.create({
    container: { 
        backgroundColor: 'white',
        flex:1,
    },
})
export default Home
